﻿USE emple_depart;


-- Mostrar todos los campos y todos los registros de la tabla empleado

SELECT * FROM emple e;

--  Mostrar todos los campos y todos los registros de la tabla departamento

  SELECT * FROM depart d;

  -- Mostrar el número, nombre y localización de cada departamento

SELECT d.dept_no,d.dnombre,d.loc FROM depart d;

-- Datos de los empleados ordenados por número de departamento descendentemente

SELECT * FROM emple e ORDER BY e.dept_no DESC;

--  Datos de los empleados ordenados por número de departamento descendentemente y por oficio ascendente


   SELECT * FROM emple e ORDER BY e.dept_no DESC,e.oficio ASC;

  -- Datos de los empleados ordenados por número de departamento descendentemente y por apellido ascendentemente

 SELECT * FROM emple e ORDER BY e.dept_no DESC,e.apellido;

-- Mostrar el apellido y oficio de cada empleado

   SELECT e.apellido,e.oficio FROM emple e;

--  Mostrar localización y número de cada departamento

  SELECT d.loc,d.dept_no FROM depart d;

  -- Datos de los empleados ordenados por apellido de forma ascendente

  SELECT * FROM emple e ORDER BY e.apellido ASC;

  -- Datos de los empleados ordenados por apellido de forma descendente

SELECT * FROM emple e ORDER BY e.apellido DESC;

--  Mostrar los datos de los empleados cuyo oficio sea ANALISTA

  SELECT * FROM emple e WHERE e.oficio='ANALISTA';

  --  Mostrar los datos de los empleados cuyo oficio se ANALISTA y ganen más de 2000

  SELECT * FROM emple e WHERE e.oficio='ANALISTA' AND e.salario>2000;

  -- Listar el apellido de todos los empleados y ordenarlos por oficio y por nombre

 SELECT e.apellido FROM emple e ORDER BY e.oficio,e.apellido;

--  Seleccionar de la tabla EMPLE los empleados cuyo apellido no termine por Z. Listar todos los campos de la tabla empleados

  SELECT * FROM emple e WHERE e.apellido NOT LIKE '%Z';

  -- Mostrar el código de los empleados cuyo salario sea mayor que 2000

    SELECT e.emp_no FROM emple e WHERE e.salario>2000;

    -- Mostrar los códigos y apellidos de los empleados cuyo salario sea menor que 2000

     SELECT e.emp_no,e.apellido FROM emple e WHERE e.salario<2000;

    -- Mostrar los datos de los empleados cuyo salario esté entre 1500 y 2500

      SELECT * FROM emple e WHERE e.salario  BETWEEN 1500 AND 2500;

      --  Seleccionar el apellido y oficio de los empleados del departamento número 20

         SELECT e.apellido,e.oficio FROM emple e WHERE e.dept_no=20;

    -- Mostrar todos los datos de empleados cuyos apellidos comiencen por m o por n ordenados por apellido de forma ascendente.

     SELECT * FROM emple e WHERE e.apellido LIKE 'm%' OR e.apellido LIKE 'n%' ORDER BY e.apellido ASC;

    -- Seleccionar los datos de los empleados cuyo oficio sea VENDEDOR. Mostrar los datos ordenados por apellido de forma ascendente.

     SELECT * FROM emple e WHERE e.oficio='VENDEDOR' ORDER BY e.apellido ASC;

    -- Mostrar los empleados cuyo departamento sea 10 y cuyo oficio sea ANALISTA. Ordenar el resultado por apellido y oficio de forma ascendente

   SELECT e.apellido FROM emple e WHERE e.dept_no=10 AND e.oficio='ANALISTA' ORDER BY e.apellido ASC,e.oficio ASC; 

-- Realizar un listado de los distintos meses en que los empleados se han dado de alta

   SELECT DISTINCT MONTH(e.fecha_alt) FROM emple e;

--  Realizar un listado de los distintos años en los que los empleados se han dado de alta

 SELECT DISTINCT YEAR(e.fecha_alt) FROM emple e;

-- Realizar un listado de los distintos días del mes en que los empleados se han dado de alta

  SELECT DISTINCT DAY(e.fecha_alt) FROM emple e;

-- Mostrar los apellidos de los empleados que tengan un salario mayor que 2000 o que pertenezcan al departamento número 20

SELECT e.apellido FROM emple e WHERE e.salario>2000 OR e.dept_no=20;

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A o por M. Listar el apellido de los empleados

 SELECT e.apellido FROM emple e WHERE e.apellido LIKE 'A%' OR e.apellido LIKE 'M%';

-- Seleccionar de la tabla EMPLE los empleados cuyo apellido empiece por A. Listar el apellido de los empleados

 SELECT DISTINCT e.apellido FROM emple e WHERE SUBSTR(e.apellido,1,1)='a';

-- Seleccionar de la tabla EMPLE aquellas filas cuyo apellido empiece por A y el oficio tenga una E en cualquier posición. 
--  Ordenar la salida por oficio y por salario de forma descendente

   SELECT * FROM emple e WHERE e.apellido LIKE'a%' AND e.oficio LIKE '%e%' ORDER BY e.oficio,e.salario DESC;

 -- Indicar el número de empleados que hay

  SELECT COUNT(*) FROM emple e;

-- Indicar el número de departamentos que hay

  SELECT COUNT(*) FROM depart d;

  -- Contar el número de empleados cuyo oficio sea VENDEDOR

    SELECT COUNT(e.emp_no) FROM emple e WHERE e.oficio='VENDEDOR';

    -- Realizar un listado donde nos coloque el apellido del empleado
    -- y el nombre del departamento al que pertenece. Ordena el resultado por apellido.

SELECT e.apellido,d.dnombre FROM emple e JOIN depart d ON e.dept_no = d.dept_no ORDER BY apellido;

-- Realizar un listado donde nos coloque el apellido del empleado, el oficio del empleado
  -- y el nombre del departamento al que pertenece.
--  Ordenar los resultados por apellido de forma descendente.


   SELECT e.apellido,e.oficio,d.dnombre FROM emple e JOIN depart d USING(dept_no) GROUP BY e.apellido DESC;

-- Mostrar los apellidos del empleado que más gana

  SELECT e.apellido FROM emple e WHERE e.salario=(SELECT MAX(e.salario) FROM emple e);

  -- Indicar el número de empleados más el número de departamentos

 --  SELECT  (SELECT COUNT(*) FROM emple e) numemple + (SELECT COUNT(*) FROM depart d) numdepa;

  SELECT (
    SELECT COUNT(*) FROM emple e
  )+(
  SELECT count(*) FROM depart d
  );

  -- Listar el número de departamento de aquellos departamentos que tengan empleados y el número de empleados que tengan.

  
SELECT dept_no,COUNT(*) FROM emple GROUP BY dept_no;



