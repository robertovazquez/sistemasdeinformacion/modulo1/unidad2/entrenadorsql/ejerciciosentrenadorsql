﻿USE provincias;

-- 2+3

   SELECT 2+3;

-- ejercicio 1

SELECT p.provincia FROM provincias p;

-- ejercicio 2


SELECT p.provincia,p.poblacion/p.superficie  FROM provincias p;


-- ejercicio 3

SELECT SQRT(2);


-- ejercicio 4

SELECT p.autonomia,p.provincia FROM provincias p;

-- ejercicio 5

SELECT p.provincia,CHAR_LENGTH(p.provincia) FROM provincias p;

-- ejercicio 6

SELECT DISTINCT p.autonomia FROM provincias p;

-- ejercicio 7

SELECT p.provincia FROM provincias p WHERE p.autonomia=p.provincia;

-- ejercicio 8

SELECT p.provincia FROM provincias p WHERE p.provincia LIKE '%ue%';

-- ejercicio 9

  SELECT p.provincia FROM provincias p WHERE p.provincia LIKE 'A%';

--  ¿Qué provincias están en autonomías con nombre compuesto?

 SELECT p.provincia FROM provincias p WHERE p.autonomia LIKE '% %';

-- ¿Qué provincias tienen nombre compuesto?

  SELECT p.provincia FROM provincias p WHERE p.provincia LIKE '% %';

  -- ¿Qué provincias tienen nombre simple?

 SELECT p.provincia FROM provincias p WHERE p.provincia NOT LIKE '% %';

 -- Muestra las provincias de Galicia, indicando si es Grande, Mediana o Pequeña en función de si su población supera el umbral de un millón o de medio millón de habitantes.

 SELECT provincia, CASE WHEN poblacion>1e6 THEN 'Grande' WHEN
   poblacion>5e5 THEN 'Mediana' ELSE
  'Pequeña' END FROM provincias WHERE autonomia='galicia';


-- Autonomías terminadas en 'ana'

 SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia LIKE '%ana';

-- ¿Cuántos caracteres tiene cada nombre de comunidad autónoma? Ordena el resultado por el nombre de la autonomía de forma descendente

 SELECT DISTINCT autonomia,CHAR_LENGTH(autonomia)  FROM provincias
ORDER BY autonomia desc;
          
--  ¿Qué autonomías tienen provincias de más de un millón de habitantes? Ordénalas alfabéticamente

 SELECT DISTINCT p.autonomia FROM provincias p WHERE p.poblacion>1e6 ORDER BY p.autonomia;

--  Población del país

 SELECT SUM(p.poblacion) FROM provincias p;

-- ¿Cuántas provincias hay en la tabla?

  SELECT COUNT(p.provincia) numprovincia FROM provincias p;

-- ¿Qué comunidades autónomas contienen el nombre de una de sus provincias?

SELECT DISTINCT p.autonomia FROM provincias p WHERE LOCATE(p.provincia,p.autonomia)>0;

--  ¿Qué autonomías tienen nombre compuesto? Ordena el resultado alfabéticamente en orden inverso

  SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia LIKE '% %'  ORDER BY p.autonomia DESC;

  --  ¿Qué autonomías tienen nombre simple? Ordena el resultado alfabéticamente en orden inverso

   SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia NOT LIKE '% %'  ORDER BY p.autonomia DESC;

  -- ¿Qué autonomías tienen provincias con nombre compuesto? Ordenar el resultado alfabéticamente

    SELECT DISTINCT p.autonomia FROM provincias p WHERE p.provincia LIKE '% %' ORDER BY p.autonomia;

 -- Autonomías que comiencen por 'can' ordenadas alfabéticamente

   SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia LIKE '%can%' ORDER BY p.autonomia;

--  Listado de provincias y autonomías que contengan la letra ñ

SELECT DISTINCT p.provincia FROM provincias p WHERE p.provincia LIKE '%ñ%' COLLATE utf8_bin
UNION
SELECT DISTINCT p.autonomia FROM provincias p WHERE p.autonomia LIKE '%ñ%' COLLATE utf8_bin;

-- Superficie del país

 SELECT SUM(p.superficie) total  FROM provincias p;

-- En un listado alfabético, ¿qué provincia estaría la primera?

SELECT MIN(p.provincia) FROM provincias p;

-- ¿Qué provincias tienen un nombre más largo que el de su autonomía?

 SELECT   p.provincia FROM provincias p WHERE CHAR_LENGTH(p.autonomia)< CHAR_LENGTH(p.provincia);

-- ¿Cuántas comunidades autónomas hay?

 SELECT COUNT( DISTINCT p.autonomia)  FROM provincias p;

--  ¿Cuánto mide el nombre de provincia más largo?

     SELECT MAX(CHAR_LENGTH(p.provincia)) FROM provincias p;

-- ¿Cuánto mide el nombre de autonomía más corto?
  
      SELECT MIN(CHAR_LENGTH(p.autonomia)) FROM provincias p;

--  Población media de las provincias entre 2 y 3 millones de habitantes sin decimales

   SELECT ROUND(AVG(p.poblacion)) FROM provincias p WHERE p.poblacion BETWEEN 2e6 AND 3e6;

  -- Listado de autonomías cuyas provincias lleven alguna tilde en su nombre

    SELECT DISTINCT  p.autonomia FROM provincias p WHERE provincia LIKE '%á%' COLLATE utf8_bin OR provincia LIKE '%é%' COLLATE utf8_bin OR provincia LIKE '%í%' COLLATE utf8_bin
    OR provincia LIKE '%ó%' COLLATE utf8_bin OR provincia LIKE '%ú%' COLLATE utf8_bin ;

 --  Provincia más poblada

  SELECT  p.provincia FROM provincias p WHERE p.poblacion=( SELECT MAX(p.poblacion) FROM provincias p);

  --  Provincia más poblada de las inferiores a 1 millón de habitantes

SELECT p.provincia FROM provincias p WHERE p.poblacion=( SELECT MAX(p.poblacion) FROM provincias p WHERE p.poblacion<1e6);

--  Provincia menos poblada de las superiores al millón de habitantes

SELECT p.provincia FROM provincias p WHERE p.poblacion=( SELECT MIN(p.poblacion) FROM provincias p WHERE p.poblacion>1e6);

-- ¿En qué autonomía está la provincia más extensa?

 SELECT p.autonomia FROM provincias p WHERE p.superficie=(SELECT MAX(p.superficie) FROM provincias p);

-- ¿Qué provincias tienen una población por encima de la media nacional?

  SELECT p.provincia FROM provincias p WHERE p.poblacion>(SELECT AVG(p.poblacion) FROM provincias p);

  -- Densidad de población del país

SELECT (
  SELECT SUM(p.poblacion) FROM provincias p
)/(
  SELECT SUM(p.superficie) FROM provincias p
);

-- ¿Cuántas provincias tiene cada comunidad autónoma?

SELECT p.autonomia, COUNT(p.provincia) FROM provincias p GROUP BY p.autonomia;

-- Obtén el listado de autonomías (una línea por autonomía),
 -- junto al listado de sus provincias en una única celda.
 -- Recuerda que, tras una coma, debería haber un espacio en blanco.


    SELECT p.autonomia,GROUP_CONCAT(p.provincia SEPARATOR ', ')
    FROM provincias p GROUP BY p.autonomia;

-- ¿Cuántas provincias con nombre compuesto tiene cada comunidad autónoma?

  SELECT p.autonomia,COUNT(*) FROM provincias p WHERE p.provincia LIKE '% %' GROUP BY p.autonomia;

  -- Listado del número de provincias por autonomía ordenadas de más a menos provincias
  -- y por autonomía en caso de coincidir

 SELECT DISTINCT COUNT(*),p.autonomia FROM provincias p GROUP BY p.autonomia ORDER BY COUNT(*) DESC,p.autonomia ASC;

-- Autonomías uniprovinciales

  

  --  ¿Qué autonomía tiene 5 provincias?


SELECT  p.autonomia FROM provincias p  GROUP BY p.autonomia HAVING COUNT(*)=5;


-- Población de la autonomía más poblada










   
